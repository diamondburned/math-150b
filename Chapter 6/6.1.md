# 6.1. Integration by Parts

$$
\begin{aligned}
&\int \sin kx \ dx &=& - \frac1k \cos kx + C \\
&\int \cos kx \ dx &=& \frac1k \sin kx + C \\
&\int e^{kx}  \ dx &=& \frac1k e^{kx} + C \\
\end{aligned}
$$

### Formula

$$
\begin{aligned}
(uv)' &= u'v + uv' \\
\int d(uv) &= \int v \cdot du + \int u \cdot dv \\
\int u \ dv &= uv - \int v \ du
\end{aligned}
$$

### Example

$$
\begin{aligned}
\text{Let } u &= x \text{ and } dv = \sin(3x) dx \\[1.5em]
du &= 1 \ dx \\
 v &= \int dv = -\frac13 \cos (3x) \\[1.5em]
&\int x \sin(3x) \ dx \\
&= uv - \int v \ du \\
&= -\frac13 x \cos (3x) - \int -\frac13 \cos(3x) \cdot 1 \ dx \\
&= -\frac13 x \cos (3x) + \frac13 \int \cos(3x) \ dx \\
&= -\frac13 x \cos (3x) + \frac19 \sin (3x) + C \\
\end{aligned}
$$

---

$$
\begin{aligned}
\text{Let } u &= \ln x \text{ and } dv = x^4 \ dx \\[1.5em]
du &= \frac1x \\
 v &= \int dv = \frac15 x^5 \\[1.5em]
&\int x^4 \ln x \ dx \\
&= uv - \int v du \\
&= \frac15 x^5 \ln x - \int \frac16 x^5 \cdot \frac1x \ dx \\
&= \frac15 x^5 \ln x - \frac15 \int x^4 \ dx \\
&= \frac15 x^5 \ln x - \frac1{25} x^5 + C
\end{aligned}
$$

---

Note: $\int \ln x$ is unknown and is therefore the incorrect choice for $dv$:

$$
\begin{aligned}
\int &\text{(polynomial)}& \cdot \ &\text{(sin/cos/exp)}& \ dx \\
&u& &dv& \\[1em]
\int &\text{(polynomial)}& \cdot \ &\text{(log/inverse trig)}& \ dx \\
&dv& &u& \\
\end{aligned}
$$

---

$$
\begin{aligned}
\text{Ex. 5.} \\[1.5em]
&\int_0^1 \tan^{-1} x \ dx \\
&&\text{Let } u &= \tan^{-1} x \text{ and } dv = 1 \ dx \\
&&du &= \frac1{1+x^2} \ dx \\
&&v &= x \\
\int \tan^{-1} x \ dx &= uv - \int v \ du \\
&= x \tan^{-1} x - \int \frac{x}{1+x^2} \ dx \\
&& \text{Let } y &= 1 + x^2 \\
&& dy &= 2x \ dx \\
&& dx &= \frac1{2x} \ dy \\
&= x \tan^{-1} x - \int \frac{x}y \cdot \frac{1}{2x} \ dy \\
&= x \tan^{-1} x - \frac12 \int \frac1y \ dy \\
&= x \tan^{-1} x - \frac12 \ln |y| \\
&= x \tan^{-1} x - \frac12 \ln |1+x^2| \\
&\int_0^1 \tan^{-1} x \ dx \\
&= \left[x \tan^{-1} x - \frac12 \ln |1+x^2|\right]_0^1 \\
&= \frac\pi4 - \frac12 \ln2
\end{aligned}
$$

---

$$
\begin{aligned}
&\int e^{3x} \sin(2x) \ dx \text{ (A)} \\
&&\text{Let } u &= e^{3x} \\
&&\text{and } dv &= \sin(2x) \ dx \\
&&du &= 3e^{3x} \\
&&v &= -\frac12 \cos(2x) \\
A &= uv - \int v \ du \\
A &= -\frac12 e^{3x} \cos(2x) - \int -\frac32 e^{3x} \cos(2x) \ dx \\
A &= -\frac12 e^{3x} \cos(2x) + \frac32 \int e^{3x} \cos(2x) \ dx \text{ (B)} \\
A &= -\frac12 e^{3x} \cos(2x) + \frac32 B \\[1.5em]
B &= \int e^{3x} \cos(2x) \ dx \\
&&\text{Let } u &= e^{3x} \\
&&\text{and } dv &= \cos(2x) \ dx \\
&&du &= 3e^{3x} \\
&&v &= \frac12 \sin(2x) \\
B &= uv - \int v \ du \\
B &= \frac12 e^{3x} \sin(2x) - \int \frac32 e^{3x} \sin(2x) \ dx \\
B &= \frac12 e^{3x} \sin(2x) - \frac32 \int e^{3x} \sin(2x) \ dx \text{ (A)} \\
B &= \frac12 e^{3x} \sin(2x) - \frac32 A \\[1.5em]
A &= -\frac12 e^{3x} \cos(2x) + \frac32 \left( \frac12 e^{3x} \sin(2x) - \frac32 A \right) \\
A &= -\frac12 e^{3x} \cos(2x) + \frac34 e^{3x} \sin(2x) - \frac94 A \\
A + \frac94 A &= -\frac12 e^{3x} \cos(2x) + \frac34 e^{3x} \sin(2x) \\
\frac{13}4 A  &= -\frac12 e^{3x} \cos(2x) + \frac34 e^{3x} \sin(2x) \\
A &= \frac4{13} \left( -\frac12 e^{3x} \cos(2x) + \frac34 e^{3x} \sin(2x) \right) \\
\end{aligned}
$$