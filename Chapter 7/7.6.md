# 7.6

Moment of the system about the y-axis:

$$
\begin{aligned}
M_y = \sum_{i=1}^n m_i x_i
\end{aligned}
$$

Moment of the system about the x-axis:

$$
\begin{aligned}
M_x = \sum_{i=1}^n m_i y_i
\end{aligned}
$$

## Coordinate for the Center of Mass

$$
\begin{aligned}
\bar x &= \frac{M_y}{m} \\[1.25em]
\bar y &= \frac{M_x}{m}
\end{aligned}
$$

## Curves

Formula for the center of mass of region $R$ that lies between two curves $y = f(x)$ and $y = g(x)$ where $f(x) \geq g(x)$, then the centroid of $R$ is $(\bar x, \bar y)$, where:

$$
\begin{aligned}
\bar x &= \frac1A \int_a^b x [f(x) - g(x)] \ dx \\
\bar y &= \frac1A \int_a^b \frac12 \left\{[f(x)]^2 - [g(x)]^2\right\} \ dx
\end{aligned}
$$

---

$$
\begin{aligned}
y &= \sqrt{r^2 - x^2}, \bar x = ?, \bar y = ? \\[2em]

A &= \frac12 \pi r^2 \\
\bar y &= \frac1{\frac12 \pi r^2} \int_{-r}^r \frac12 \left[\left(\sqrt{r^2 - x^2}\right)^2 - 0^2\right] \ dx \\
&= \frac2{\pi r^2} \cdot \frac12 \int_{-r}^r (r^2 - x^2) \dx \\
&= \frac1{\pi r^2} \left[r^2x - \frac13 x^3\right]_{-r}^r \\
&= \frac1{\pi r^2} \left[(r^3 - \frac13 r^3) - (-r^3 + \frac13 r^3)\right] \\
&= \frac1{\pi r^2} \cdot \frac43 r^3 \\
\bar y &= \frac{4}{3\pi} r
\end{aligned}
$$

**Note:** by symmetry, we know that the centroid is on the y-axis, so $\bar x = 0$.

Therefore, centroid is at $(0, \frac4{3\pi} r)$.

---

$$
\begin{aligned}
y &= x - x^2 \\
dy &= 1 - 2x \\[1.5em]

A &= \int_0^1 \left(x-x^2\right) \ dx \\
  &= \frac12 x^2 - \frac13 x^3 \Bigl|_0^1 \\
  &= \frac16 \\[1.5em]

\bar x &= \frac1{\frac16} \int_0^1 x \left(x - x^2\right) dx \\
       &= 6 \int_0^1 \left(x^2 - x^3\right) \ dx \\
       &= 6 \left[\frac13 x^3 - \frac14 x^4\right]_0^1 \\
       &= 6 \left(\frac1{12}\right) \\
       &= \frac12 \\[1.5em]

\bar y &= \frac1{\frac16} \int_0^1 \frac12 \left[x ^2 - \left(x^2\right)^2 \right] dx \\
       &= 6 \cdot \frac12 \int_0^1 \left(x^2 - x^4\right) \ dx \\
       &= 3 \left[\frac13 x^3 - \frac15 x^5\right] \\
       &= 3 \left[\frac13 - \frac15\right] \\
       &= 3 \cdot \frac{2}{15} \\
       &= \frac25
\end{aligned}
$$

Centroid: $\left(\frac12, \frac25\right)$

---

A torus is formed by rotating a circle of radius $r$ about a line in the plane of the circle that is a distance $R > r$ from the center of the circle. Find the volume of the torus.

![](2021-03-04-11-44-05.png)

$$
\begin{aligned}

\end{aligned}
$$