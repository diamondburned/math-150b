# Appendix F - Linear Equations

- A **diiferential equation** is an equation that contains derivatives of an unknown function.
- The order of the highest derivative in the DE is called the **order of the DE**

Example:

$$
\begin{aligned}
y'' + 2xy^2 &= \sin(xy) \\
x^2 y' + ln(x+y) &= 2y + 1 
\end{aligned}
$$

A **first-order linear** DE is of the form:

$$
\begin{aligned}
\frac{dy}{dx} + P(x) y = Q(x)
\end{aligned}
$$

To solve, multiply both sides by the **integrating factor**

$$
\begin{aligned}
Ix = e^{\int P(x) \ dx} 
\end{aligned}
$$

and integrate both sides.

---

$$
\begin{aligned}
y' + 3x^2 y &= 6x^2 \\[1.5em]
I(x) &= e^{\int P(x) \ dx} = e^{\int 3x^2 \ dx} = e^{x^3} \\[1.5em]
e^{x^3} y' + 3x^2 e^{x^3} y &= 6x^2 e^{x^3} \\
\frac{d}{dx} \left(e^{x^3} y\right) &= 6x^2 e^{x^3} \\
e^{x^3} &= \int 6x^2 e^{x^3} \ dx \\
e^{x^3} &= 2 e^{x^3} + C \\
y &= 2 + Ce^{-x^3}
\end{aligned}
$$

---

$$
\begin{aligned}
y' + 2xy &= x \\[1.5em]
I(x) &= e^{\int P(x) \ dx} = e^{\int 2x} = e^{x^2} \\[1.5em]
e^{x^2} y' + 2xe^{x^2}y &= xe^{x^2} \\
\frac{d}{dx} (e^{x^2} y) &= xe^{x^2} \\
e^{x^2} y &= \int xe^{x^2} \ dx \\
e^{x^2} y &= \frac12 e^{x^2} + C \\
y &= \frac12 + Ce^{-x^2}
\end{aligned}
$$

---

$$
\begin{aligned}
x^2 y' + xy &= 1, x > 0, y(1) = 2 \\
\implies y' + \frac1x y &= \frac1{x^2} \\[1.5em]
I(x) &= e^{\int \frac1x \ dx} = e^{\ln x} = x \\[1.5em]
xy' + y &= \frac1x \\
\frac{d}{dx} (xy) &= \frac1x \\[1.5em]
xy &= \int \frac1x dx = \ln x + C \\
xy &= \ln x + C \\[1.5em]
y(1) &= 2 \\
&\implies (1)(2) = \ln 1 + C \\
&\implies C = 2 \\
xy &= \ln x + 2 \\
&\implies y = \frac{\ln x}{x} + \frac2x
\end{aligned}
$$

---

$$
\begin{aligned}
&&xy' - 2y &= x^3, \ x > 0 \\
&\text{1) Divide by x: }& \\ && y' - \frac2x y &= x^2  \\
&\text{2)              }& \\ && I(x) &= e^{\int -\frac2x \ dx} = e^{-2 \ln x} = e^{\ln x^{-2}} = x^{-2} \\
&{\text{3) Multiply by } x^{-2}: }& \\ && x^{-2}y' - 2x^{-3}y &= 1 \\ 
&&\ddx \left(x^{-2}y\right) &= 1 \\
&\text{4) Integrate }& \\
&&x^{-2}y &= \int 1 dx = x+c \\
&&y &= x^3 + cx^2 
\end{aligned}
$$

---

Find an exprpession for the current in a circuit where:

$$
\begin{aligned}
L \frac{\text{d}I}{\text{d}t} + RI = E(t)
\end{aligned}
$$

- The resistance is 12 $\Omega$.
- The inductance is 4H.
- A battery gives a constant voltage of 60V.
- The switch is turned on when $t = 0 \implies I(0) = 0$.

$$
\begin{aligned}
R &= 12 \\
L &= 4 \\
E(t) &= 60 \\
\end{aligned}
$$

$$
\begin{aligned}
4 \frac{dI}{dt} + 12 I &= 60 \\
\frac{dI}{dt} + 3 I &= 15 \\[1em]
I(t) &= e^{\int 3 \ dt} = e^{3t} \\[1em]
e^{3t} \frac{dI}{dt} + 3e^{3t}I &= 12e^{3t} \\
e^{3t} I &= \int 15 e^{3t} \ dt \\
e^{3t} I &= \frac{15}{3} e^{3t} + C \\[1em]
I(0) = 0 &\implies e^0 \cdot 0 = 5e^0 + C \implies C = -5 \\
e^{3t} I &= 5 e^{3t} - 5 \implies I = 5 - \frac{5}{3^{3t}} \\
\limfn{t}{\infty} (5 - \frac{5}{e^{3t}}) &= 5
\end{aligned}
$$

---

A typical mixing problem involves a tank of fixed capacity filled with a thoroughly
mixed solution of some substance, such as salt.

- A solution of a given concentration enters the tank at a fixed rate.
- The mixture, thoroughly stirred, leaves at a fixed rate, which may
  differ from the entering state.

Let $y(t)$ be the amount of substance in the tank at the time $t$.

Then $y'(t)$ is the rate at which the substance is being added minus the rate at
which it is being removed.

A tank contains 20$kg$ of salt dissolved in 5000$L$ of water.

- Brine that contains 0.03$kg$ of salt per liter of water enters the tank at a rate
  of 25$L$/min.
- The solution is kept thoroughly mixed and drains from the tank at the same time.
- How much salt remains in the tank after half an hour?

Let $y(t)$ be the amount of salt in the tank after $t$ minutes.

$$
\begin{aligned}
\frac{dy}{dt} &= &&\ \text{(rate in)} &&- &&\ \text{(rate out)} \\
\frac{dy}{dt} &= &&\left(0.03 \ \frac{\text{kg}}{\text{L}}\right) \left(25 \frac{\text{L}}{\text{min}}\right)
             &&- &&\left(\frac{y}{5000} \frac{\text{kg}}{\text{L}}\right) \left(25 \frac{\text{L}}{\text{min}}\right) \\
\end{aligned}
$$

$$
\begin{aligned}
0.75 - \frac{1}{200}y \\[1em]
y(0) &= 20 \\[1em]
\frac{dy}{dt} + \frac{1}{200}y &= 0.75 \\[1em]
I(t) &= e^{\int \frac{1}{200}dt} = e^{\frac{1}{200}t} \\[1em]
e^{\frac{1}{200}t} \frac{dy}{dt} + e^{\frac{1}{200}t} \frac{1}{200} y &= 0.75 e^{\frac{1}{200}t} \\
\int \frac{d}{dt} \left(e^{\frac1{200}t} y \right) &= \int 0.75 e^{\frac{1}{200}t} \\
e^{\frac1{200}t} y &= (0.75)200 e^{\frac1{200}t} + C \\[1em]
y(0) = 20 \implies e^0 (20) &= 150 e^0 + C \\
C &= -130 \\
e^{\frac1{200}t} y &= 150 e^{\frac1{200}t} - 130 \\
y &= 130 - 130 e^{-\frac1{200}t}
\end{aligned}
$$

---

$$
\begin{aligned}
\frac{dy}{dt} &= &&\text{(rate in)} &&- &&\ \text{(rate out)} \\
\frac{dy}{dt} &= &&0
             &&- &&\left(\frac{y}{1000} \frac{\text{kg}}{\text{L}}\right) \left(10 \frac{\text{L}}{\text{min}}\right) \\
\frac{dy}{dt} &= &&-\frac{1}{100}y, \ y(0) = 15
\end{aligned}
$$