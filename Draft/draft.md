$$
\begin{aligned}
\int f_6(x) &= \int \frac{-5x}{x^2 - 5x + 6} \ dx \\
&= \int \frac{A}{x-3} + \frac{B}{x-2} \ dx \\[1.5em]
\end{aligned}
$$

$$
\begin{aligned}
&= \frac1{3\left(\frac{u^2}{3} + 1\right)} \\
&= \frac1{3\left(\frac{u^2}{\sqrt{3}^2} + 1\right)} \\
\end{aligned}
$$

$$
\begin{aligned}
\pi \int_{-\frac\pi3}^{\frac\pi3} (5-3)^2 - (3+\sec x-3)^2 \ dx
\end{aligned}
$$