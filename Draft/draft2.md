$$
\begin{aligned}
f_2(x) = &\ \frac12 \sqrt{1+x} \\
       = &\ \frac12 \left(1+x\right)^\frac12 \\
f_2(x)' = &\ \frac1{4\sqrt{1+x}} \\[2em]

L = &\ \int_0^1 2 \pi \left(\frac12 \sqrt{1+x}\right) \sqrt{1+\left(\frac{1}{4 \sqrt{1+x}}\right)^2} \ dx \\
  = &\ \pi \int_0^1 \sqrt{1+x} \cdot \frac14 \sqrt{16 + \frac1{1+x}} \ dx \\
  = &\ \frac\pi4 \int_0^1 \sqrt{(1+x) \left(16 + \frac1{1+x}\right)} \ dx \\
  = &\ \frac\pi4 \int_0^1 \sqrt{16(1+x) + 1} \ dx \\
  = &\ \frac\pi4 \int_0^1 \sqrt{17 + 16x} \ dx \\[2em]
&\ \int \sqrt{17 + 16x} \ dx \\
&&\text{Let } u &= 17 + 16x \\
&&du &= 16 \ dx \\
= &\ \int \sqrt{u} \ \frac{du}{16} \\
= &\ \frac1{16} \int \sqrt{u} \ du \\
= &\ \frac1{16} \cdot \frac23 u^\frac32 \\
= &\ \frac1{24} u^\frac32 \\
= &\ \frac1{24} (17+16)^\frac32 \\[2em]
\implies &\ \frac\pi4 \int_0^1 \sqrt{17 + 16x} \ dx \\
       = &\ \frac\pi4 \left[\frac1{24} (17+16x)^\frac32\right]_0^1 \\
       = &\ \frac\pi{96} \left[(17+16x)^\frac32\right]_0^1 \\
       = &\ \frac\pi{96} \left[(17+16)^\frac32 - 17^\frac32\right] \\
       = &\ \frac{33^\frac32 \pi}{96} - \frac{17^\frac32 \pi}{96} \\[1em]
       \approx &\ 3.910 \\[2em]

V = &\ \int_0^1 \pi [f_2(x)]^2 \ dx \\
  = &\ \pi \int_0^1 \left(\frac12 \sqrt{1+x}\right)^2 \ dx \\
  = &\ \pi \int_0^1 \left(\frac14 (1+x)\right) \ dx \\
  = &\ \pi \frac14 \int_0^1 1 + x \ dx \\
  = &\ \frac\pi4 \left(\int 1 dx + \int x dx\right) \\
  = &\ \frac\pi4 \left(x + \frac12 x^2\right) \\
  = &\ \left[\frac{2\pi x + \pi x^2}8\right]_0^1 \\
  = &\ \frac{2 \pi + \pi}8 - \frac{0}{8} \\
  = &\ \frac{3\pi}8 \\[1em]
  \approx &\ 1.178
\end{aligned}
$$

---

Surface I

$$
\begin{aligned}
f_1(x) &= \sin \left(\frac\pi2 x\right) \\[1.5em]

f_1'(x) &= \cos\left(\frac{\pi x}2\right) \cdot \ddx \left(\frac{\pi x}{2}\right) \\
                                    &= \cos\left(\frac{\pi x}2\right) \left(\frac{\pi}{2}\right) \\
                                    &= \frac{\pi \cos\left(\frac{\pi x}{2}\right)}{2} \\[2em]

D = &\ \int_0^l 2 \pi f(x) \sqrt{1 + [f'(x)]^2} \ dx \\
  = &\ \int_0^1 2 \pi \sin\left(\frac{\pi x}{2}\right) \sqrt{1 + \left(\frac{\pi \cos \left(\frac{\pi x}{2}\right)}{2}\right)^2} \ dx \\[2em]

\\
    &\ \int 2 \pi \sin\left(\frac{\pi x}{2}\right) \sqrt{1 + \left(\frac{\pi \cos \left(\frac{\pi x}{2}\right)}{2}\right)^2} \ dx \\
  = &\ 2\pi \int \sin\left(\frac{\pi x}2\right) \sqrt{\frac{\pi^2 \cos^2 \left(\frac{\pi x}2\right)}4 + 1} \ dx \\
  = &\ \pi  \int \sin\left(\frac{\pi x}2\right) \sqrt{\pi^2 \cos^2 \left(\frac{\pi x}2\right) + 4} \ dx \\
        &&\text{Let } v &= \frac{\pi x}2 \\
        &&           dv &= \frac{\pi}2 \ dx \\
  \implies &\ 2 \int \sin u \sqrt{\pi^2 \cos^2 u + 4} \ dx \\
  = &\ -2 \int \sqrt{\pi^2 v^2 + 4 } \ dv \\
        &&\text{Let } v &= \frac2\pi \tan \theta \\
        &&           dv &= 2\pi \sec^2 \theta d \theta \\
  = &\ -2 \int \frac2\pi \sec^2 \theta \sqrt{\pi^2 \frac4{\pi^2} \tan^2 \theta + 4} \ d\theta \\
  = &\ -2 \int \frac2\pi \sec^2 \theta \sqrt{4(\tan^2 \theta + 1)} \ d\theta \\
  = &\ -2 \int \frac2\pi \sec^2 \theta \sqrt{4 \sec^2 \theta} \ d\theta \\
  = &\ -2 \int \frac2\pi \sec^2 \theta \cdot2 \sec \theta \ d\theta \\
  = &\ -\frac8\pi \int \sec^3 \theta \ d \theta \\
  = &\ -\frac8\pi \left(\int \sec^2 \theta \ d\theta + \int \sec \theta \ d\theta\right) \\
  = &\ -\frac8\pi \left(\frac12 \sec\theta \tan\theta + \frac12 \ln\left|\tan \theta + \sec\theta\right| \right) \\
        &&\text{Sub. } \theta &= \arctan\left(\frac{\pi v}2\right) \\
  = &\ -v \sqrt{\frac{\pi^2 v^2}4 + 1} - \frac{2 \ln\left(\sqrt{\frac{\pi^2 v^2}4+1}+\frac{\pi v}2\right)}\pi \\
        &&\text{Sub. } v &= \cos u \\
  = &\ -\frac{2 \ln\left(\sqrt{\frac{\pi^2 \cos^2 u}4 + 1}\right)}\pi - 2 \cos u \sqrt{\frac{\pi^2 \cos^2 u}4 + 1} \\
        &&\text{Sub. } u &= \frac{\pi x}2 \\
  = &\ -\frac{2 \ln\left(\sqrt{\frac{\pi^2 \cos^2 \left(\frac{\pi x}2\right)}4} + \frac{\pi \cos\left(\frac{\pi x}2\right)}2\right)}\pi - 2 \cos \left(\frac{\pi x}2\right) \sqrt{\frac{\pi^2 \cos^2 \left(\frac{\pi x}2\right)}4 + 1} \\[2em]

    &\ \int_0^1 2 \pi \sin\left(\frac{\pi x}{2}\right) \sqrt{1 + \left(\frac{\pi \cos \left(\frac{\pi x}{2}\right)}{2}\right)^2} \ dx \\
  = &\ -\frac{2 \ln\left(\sqrt{\frac{\pi^2 \cos^2 \left(\frac{\pi x}2\right)}4} + \frac{\pi \cos\left(\frac{\pi x}2\right)}2\right)}\pi - 2 \cos \left(\frac{\pi x}2\right) \sqrt{\frac{\pi^2 \cos^2 \left(\frac{\pi x}2\right)}4 + 1} \Bigg|_0^1 \\[1em]
  \approx &\ 5.295 \\[2em]

V = &\ \int_0^1 \pi \left[\sin \left(\frac\pi2 x\right)\right]^2 \ dx \\
        &&\text{Let } u &= \frac\pi2 x \\
        &&           du &= \frac\pi2 \\
  = &\ \int 2 \sin (u)^2 \ du \\
  = &\ 2 \int \sin(u^2) \ du \\
  = &\ 2 \int \frac{1 - \cos(2u)}2 \ du \\
  = &\ 2 \cdot \frac12 \cdot \int 1 - \cos(2u) \ du \\
  = &\ \int 1 - \cos(2u) \ du \\
  = &\ \int 1 \ du - \int \cos(2u) \ du \\
  = &\ u - \frac{\sin(2u)}2 \\
        &&\text{Sub. } u &= \frac\pi2 x \\
  = &\ \frac\pi2 x \cdot \frac{\sin\left(2 \frac\pi2 \cdot x\right)}2 \\
  = &\ \frac{\pi x - \sin(\pi x)}2 \bigg|_0^1 \\
  = &\ \frac{\pi - \sin \pi}2 - \frac{-\sin(0)}2 \\
  = &\ \frac\pi2 \\[1em]
  \approx &\ 1.571 \\
\end{aligned}
$$

---

$$
\begin{aligned}
f_3 (x) = &\ \frac52 - \sqrt{4-x^2} \\[1.5em]
f_3'(x) = &\ \frac{x}{\sqrt{4-x^2}} \\[2em]

D(x) = &\ 2 \pi \int_0^1 \left(\frac52 - \sqrt{4-x^2}\right) \sqrt{\left(\frac{x}{\sqrt{4-x^2}}\right)^2 + 1} \ dx \\
     = &\ 2 \pi \int_0^1 \left(5 - 2\sqrt{4-x^2}\right) \sqrt{-\frac1{x^2-4}} \ dx \\
     = &\ 2 \pi \int_0^1 \frac{(5 - 2 \sqrt{4-x^2})}{\sqrt{4-x^2}} \ dx \\
     = &\ 2 \pi \int_0^1 \frac{5}{\sqrt{4-x^2}} - \frac{2 \sqrt{4-x^2}}{\sqrt{4-x^2}} \ dx \\
     = &\ 2 \pi \left[\int \frac5{\sqrt{4-x^2}} \ dx - \int 2 \ dx\right]_0^1 \\
     = &\ 2 \pi \left[\int \frac5{\sqrt{4-x^2}} \ dx - 2x\right]_0^1 \\
     = &\ 2 \pi \left[-2x + 5 \arcsin\left(\frac{x}2\right)\right]_0^1 \\[1em]
     \approx &\ 3.88 \\[2em]

V(x) = &\ \int_0^1 \pi \left(\frac52 - \sqrt{4-x^2}\right)^2 \ dx \\
     = &\ \pi \int_0^1 \left(\frac52 - \sqrt{4-x^2}\right)^2 \ dx \\
     = &\ \pi \int_0^1 \frac{25}4 - 5 \sqrt{4-x^2} + 4 - x^2 \ dx \\
     = &\ \pi \left(\int_0^1 \frac{25}4 \ dx - \int_0^1 5 \sqrt{4-x^2} \ dx + \int_0^1 4 \ dx - \int_0^1 x^2 \ dx\right) \\
     = &\ \pi \left(\frac{25}4 - 10\left(\frac\pi6 + \frac{\sqrt{3}}{4}\right) + 4 - \frac13\right) \\
     = &\ -10\pi \left(\frac\pi6 + \frac{\sqrt{3}}{4}\right) + \frac{119}{12} \pi \\
     \approx &\ 1.101
\end{aligned}
$$

---

$$
\begin{aligned}
f (x) = &\ \frac12 \sqrt{1 + x} \\[1.5em]
g (x) = &\ cf(x-a) \\
      = &\ \frac65 \left(\frac12 \sqrt{1+x}\right) \\
      = &\ \frac35 \sqrt{1 + x} \\[1.5em]
g'(x) = &\ \frac32 \cdot \frac1{2\sqrt{1+x}} \\
      = &\ \frac3{10 \sqrt{1+x}} \\[2em]

D = &\ \int_0^1 2 \pi g(x) \sqrt{1 + \left(g'(x)\right)^2} \ dx \\
  = &\ \int_0^1 2 \pi \cdot \frac35 \sqrt{1+x} \cdot \sqrt{1 + \left(\frac3{10 \sqrt{1 + x}}\right)^2} \ dx \\
  = &\ \frac{6\pi}5 \int_0^1 \sqrt{1+x} \sqrt{1 + \frac9{100(x+1)}} \ dx \\
  = &\ \frac{3\pi}{25} \int_0^1 \sqrt{1 + x} \sqrt{100 + \frac9{x+1}} \ dx \\
  = &\ \frac{3\pi}{25} \int_0^1 \sqrt{100(1+x) + \frac{9(x+1)}{x+1}} \ dx \\
  = &\ \frac{3\pi}{25} \int_0^1 \sqrt{100(1+x) + 9} \ dx \\
       &&\text{Let } u &= 100(x+1) + 9 \\
       &&           du &= 100 \ dx \\
  = &\ \frac{3\pi}{25} \cdot \frac1{100} \int_0^1 \sqrt{u} \ du \\
  = &\ \frac{3\pi}{2500} \left(\frac23 u^\frac32\right)_0^1 \\
  = &\ \frac{\pi(100(x+1) + 9)^\frac32}{1250} \bigg|_0^1 \\
  = &\ \frac{\pi(209^\frac32 - 109^\frac32)}{1250} \\[1em]
  \approx &\ 4.734 \\[2em]

V = &\ \int_0^1 \pi (g(x))^2 \ dx \\
  = &\ \int_0^1 \pi \left(\frac35 \sqrt{1+x}\right)^2 \ dx \\
  = &\ \int_0^1 \pi \left(\frac{3^2 (\sqrt{1+x})^2}{5^2}\right) \\
  = &\ \int_0^1 \pi \left(\frac{9(x+1)}{25}\right) \ dx \\
  = &\ \frac{9\pi}{25} \int_0^1 x+1 \ dx \\
  = &\ \frac{9\pi}{25} \left(\frac{x^2}{2} + x\right)_0^1 \\
  = &\ \frac{9\pi}{25} \cdot \frac32 \\[.8em]
  = &\ 0.54 \pi \\[.5em]
  \approx &\ 1.696
\end{aligned}
$$